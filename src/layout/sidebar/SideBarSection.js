import React from 'react';
import SideBrand from './SideBrand'
import SideMenu from './SideMenu';

function SideBarSection(props) {
    return (
        <section className="side-bar">

            <SideBrand/>
            <SideMenu/>
            
        </section>
    );
}

export default SideBarSection;