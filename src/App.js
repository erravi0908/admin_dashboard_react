import logo from './logo.svg';
import './App.css';
import SideBarSection from './layout/sidebar/SideBarSection';
import MainContentSection from './layout/sidebar/maincontent/MainContentSection';

function App() {
  return (
    <>
      <SideBarSection/>
      <MainContentSection/>
      
    </>
  );
}

export default App;
